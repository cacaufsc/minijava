package recovery;

import parser.*;

import java.util.*;


public class First { //implementa os conjuntos first p/ alguns n.terminais

    static public final RecoverySet methoddecl = new RecoverySet();
    static public final RecoverySet vardecl = new RecoverySet();
    static public final RecoverySet classlist = new RecoverySet();
    static public final RecoverySet constructdecl = new RecoverySet();
    static public final RecoverySet statlist = new RecoverySet();
    static public final RecoverySet program = classlist;

    static {
        methoddecl.add(new Integer(MiniJavaParserConstants.INTEGER));
        methoddecl.add(new Integer(MiniJavaParserConstants.STRING));
        methoddecl.add(new Integer(MiniJavaParserConstants.IDENTIFIER));

        vardecl.add(new Integer(MiniJavaParserConstants.INTEGER));
        vardecl.add(new Integer(MiniJavaParserConstants.STRING));
        vardecl.add(new Integer(MiniJavaParserConstants.IDENTIFIER));

        classlist.add(new Integer(MiniJavaParserConstants.CLASS));

        constructdecl.add(new Integer(MiniJavaParserConstants.CONSTRUCT));

        statlist.addAll(vardecl);
        statlist.add(new Integer(MiniJavaParserConstants.IDENTIFIER)); // first do atribstat
        statlist.add(new Integer(MiniJavaParserConstants.PRINT));
        statlist.add(new Integer(MiniJavaParserConstants.READ));
        statlist.add(new Integer(MiniJavaParserConstants.RETURN));
        statlist.add(new Integer(MiniJavaParserConstants.SUPER));
        statlist.add(new Integer(MiniJavaParserConstants.IF));
        statlist.add(new Integer(MiniJavaParserConstants.LBRACE));
        statlist.add(new Integer(MiniJavaParserConstants.BREAK));
        statlist.add(new Integer(MiniJavaParserConstants.SEMICOLON));
    }
}
